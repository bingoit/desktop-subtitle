package tts;
 
import util.YamlReader;

import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * 方式4:实现Callable<T> 接口
 * 含返回值且可抛出异常的线程创建启动方式
 * @author fatah
 */
public class TtsExecute implements Callable<String>{

	private String text;
	private String appKey;
	private String token;
	private String url;

	public TtsExecute(String text) {
		YamlReader yml = new YamlReader();
		this.appKey = (String) yml.get("appkey");
		this.token = (String) yml.get("token");
		this.text = text;
		this.url = "";
	}
	public String call() throws Exception {
		System.out.println("正在执行新建线程任务");
		TtsTask demo = new TtsTask(appKey, token, url);
		demo.process(text);
		demo.shutdown();
		demo.play();
		return "线程执行结束";
	}
 
	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {

		/*	call()只是线程任务,对线程任务进行封装
			class FutureTask<V> implements RunnableFuture<V>
			interface RunnableFuture<V> extends Runnable, Future<V>
		*/
		while (true){
			System.out.println("请输出：");
			Scanner scanner = new Scanner(System.in);
			String text = scanner.next();
			if("退出".equals(text)){
				break;
			}
			TtsExecute de = new TtsExecute(text);
			FutureTask<String> task = new FutureTask(de);
			Thread th = new Thread(task);
			th.start();
			String result = task.get();
			System.out.println("线程执行结果为"+result);
		}
	}
	
}