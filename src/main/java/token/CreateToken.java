package token;

import com.alibaba.nls.client.AccessToken;
import util.YamlReader;

import java.io.IOException;

/**
 * @author siwei
 * @date 2019-10-30
 */
public class CreateToken {

    public static String accessKeyId;
    public static String accessKeySecret;

    static {
        YamlReader yml = new YamlReader();
        accessKeyId = (String) yml.get("id");
        accessKeySecret = (String) yml.get("secret");
    }

    public static void main(String[] args) {

        System.out.println("accessKeyId= "+accessKeyId+"; accessKeySecret= "+accessKeySecret);
        AccessToken accessToken = new AccessToken(accessKeyId, accessKeySecret);
        try {
            accessToken.apply();
            System.out.println("Token: " + accessToken.getToken() + ", expire time: " + accessToken.getExpireTime());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}